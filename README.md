# ABJ-BLOGS   
This is my minimalist blog app where I write about tech and software. 
The app has been hosted at: [Link](https://abhj.github.io/abj-blogs).
Sometimes I also write about other nerdy topics too. 
  
Design inspiration:  
1. Colors have been chosen from open source project https://github.com/joshdick/onedark.vim
2. I have been inspired by youtuber https://distrotube.com.  

Due credits to them.
